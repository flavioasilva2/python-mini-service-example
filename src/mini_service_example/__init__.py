__version__='0.1.0'


import json
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .config import get_settings
from .v1 import router as router_v1
from .database import (
    init_db,
    shutdown_db,
    apply_migrations
)


settings = get_settings()


app = FastAPI(
    title="Mini service example",
    description="Exemplo de mini serviço em python",
    version=__version__
)


@app.on_event("startup")
async def startup():
    """
    Callback de startup do FastAPI.
    """
    await apply_migrations()
    await init_db()


@app.on_event("shutdown")
async def shutdown():
    """
    Callback de shutdown do FastAPI.
    """
    await shutdown_db()


__cors_origins = json.loads(settings.MINI_SERVICE_EXAMPLE_CORS_ORIGINS)


app.add_middleware(
    CORSMiddleware,
    allow_origins=__cors_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(router_v1)
