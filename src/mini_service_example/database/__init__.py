"""
Módulo de banco de dados.
"""
from concurrent.futures import ProcessPoolExecutor
from functools import partial
from asyncio import (
    current_task,
    get_running_loop
)
from sqlalchemy.orm import (
    declarative_base,
    sessionmaker
)
from sqlalchemy.ext.asyncio import (
    async_scoped_session,
    create_async_engine,
    AsyncSession
)
from sqlalchemy.pool import StaticPool
import alembic.config
from ..config import get_settings


Base = declarative_base()


__ASYNC_ENGINE = None


__ASYNC_SCOPED_SESSION = None


async def get_async_scoped_session() -> async_scoped_session:
    """
    Retorna uma nova sessão.
    """
    global __ASYNC_SCOPED_SESSION
    if __ASYNC_SCOPED_SESSION is None:
        raise Exception('Você chamou init_db durante a inicialização?')
    return __ASYNC_SCOPED_SESSION


async def shutdown_db() -> None:
    """
    Encerra o banco de dados no encerramento do servidor.
    """
    global __ASYNC_ENGINE
    global __ASYNC_SCOPED_SESSION
    await __ASYNC_SCOPED_SESSION.close()
    await __ASYNC_ENGINE.dispose()


async def init_db() -> None:
    """
    COnfigura o banco de dados durante a inicialização do servidor.
    """
    global __ASYNC_ENGINE
    global __ASYNC_SCOPED_SESSION

    settings = get_settings()

    if 'sqlite+aiosqlite' in settings.MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING:
        __ASYNC_ENGINE = create_async_engine(
            settings.MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING,
            echo=settings.MINI_SERVICE_EXAMPLE_ORM_ECHO,
            connect_args={"check_same_thread": False},
            poolclass=StaticPool
        )

    elif 'postgresql+asyncpg' in settings.MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING:
        __ASYNC_ENGINE = create_async_engine(
            settings.MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING,
            echo=settings.MINI_SERVICE_EXAMPLE_ORM_ECHO
        )

    else:
        raise ValueError("The MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING environment variable must use one of 'sqlite+aiosqlite' | 'postgresql+asyncpg') scheme")

    __ASYNC_SCOPED_SESSION = async_scoped_session(
        sessionmaker(
            bind=__ASYNC_ENGINE,
            class_=AsyncSession,
            autocommit=False,
            autoflush=False
        ),
        current_task
    )

    from .models.todo import Todo
    from .models.todo_history import TodoHistory

    async with __ASYNC_ENGINE.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

async def apply_migrations() -> None:
    """
    Aplica as migrações de banco de dados.
    """
    settings = get_settings()
    db_path = settings.MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING

    alembicArgs = [
        '--raiseerr',
        '-x', f'dbPath={db_path}',
        'upgrade', 'head'
    ]

    loop = get_running_loop()
    main_partial = partial(alembic.config.main, argv=alembicArgs)

    with ProcessPoolExecutor(max_workers=1) as pool:
        """
        Executa as migrations em um processo separado,
        pois o alembic necessita do seu próprio loop
        de eventos.
        """
        await loop.run_in_executor(pool, main_partial)

