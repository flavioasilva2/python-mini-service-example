from sqlalchemy import (
    Column,
    DateTime,
    String,
    Integer,
    ForeignKey
)
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from .. import Base


class TodoHistory(Base):
    __tablename__      = 'mse_todo_history'
    __mapper_args__    = {'eager_defaults': True}
    timestamp          = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
    id                 = Column(Integer, primary_key=True)
    title              = Column(String(50), nullable=False)
    description        = Column(String(500), nullable=False)
    todo_id            = Column(Integer, ForeignKey('mse_todo.id'), nullable=False)
    todo               = relationship(
        "Todo", back_populates='todo_history', uselist=False, foreign_keys=[todo_id]
    )




    def __repr__(self) -> str:
        return f"<TodoHistory id={self.id} timestamp={self.timestamp} title={self.title} description={self.description} todo_id={self.todo_id}>"
