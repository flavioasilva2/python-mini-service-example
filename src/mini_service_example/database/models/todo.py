from sqlalchemy import (
    Column,
    Integer,
)
from sqlalchemy.orm import relationship
from .. import Base


class Todo(Base):
    __tablename__      = 'mse_todo'
    __mapper_args__    = {'eager_defaults': True}
    id                 = Column(Integer, primary_key=True)
    todo_history       = relationship('TodoHistory', back_populates='todo')

    def __repr__(self) -> str:
        return f"<Todo id={self.id}>"
