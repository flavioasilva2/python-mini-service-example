import os
from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING: str = os.environ.get('MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING', 'sqlite+aiosqlite:///mini_service_example.db')
    """
    postgresql+asyncpg or sqlite+aiosqlite
    Ex:
    'sqlite+aiosqlite:///test.db'
    'postgresql+asyncpg://test:test123@localhost:5432/test_database'
    """
    MINI_SERVICE_EXAMPLE_ORM_ECHO: bool = os.environ.get('MINI_SERVICE_EXAMPLE_ORM_ECHO', 'true').lower() in ('true',)
    MINI_SERVICE_EXAMPLE_CORS_ORIGINS: str = os.environ.get('MINI_SERVICE_EXAMPLE_CORS_ORIGINS', '["http://localhost:8080/", "http://127.0.0.1:8080/"]')


class DevSettings(Settings):
    pass


class ProdSettings(Settings):
    pass


@lru_cache()
def get_settings() -> Settings:
    """
    Recupera configurações do ambiente
    """
    fast_api_env = os.environ.get('FAST_API_ENV', 'production')
    if fast_api_env == 'production':
        return ProdSettings()
    if fast_api_env == 'devlopment':
        return DevSettings()
    raise ValueError('FAST_API_ENV must be "production" or "devlopment"')
