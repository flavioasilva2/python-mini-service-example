from fastapi import status, Response
from ..models.todo import (
    CreateTodo,
    Status,
    StatusEnum,
    UpdateTodo,
    DeleteTodo
)
from ..controllers.todo import (
    create_todo,
    get_todo_by_id,
    get_todos,
    update_todo_by_id,
    delete_todo_by_id
)


async def post_todo(todo: CreateTodo):
    await create_todo(todo)

    return Status(
        status = StatusEnum.CREATED
    )


async def get_todo(todo_id: int, response: Response):
    todo = await get_todo_by_id(todo_id)

    if todo is None:
        response.status_code=status.HTTP_400_BAD_REQUEST
        return Status(
            status=StatusEnum.FAIL,
            msg=f'Fail to recovery todo with id {todo_id}'
        )

    return Status(
        status=StatusEnum.READED,
        todos=[todo]
    )


async def get_all_todos(response: Response):
    todos = await get_todos()

    if len(todos) == 0:
        response.status_code=status.HTTP_404_NOT_FOUND
        return Status(
            status=StatusEnum.FAIL,
            msg="There are no todos in the database."
        )

    return Status(
        todos=todos,
        status=StatusEnum.READED
    )


async def update_todo(todo: UpdateTodo):
    await update_todo_by_id(todo)

    return Status(
        status = StatusEnum.UPDATED
    )
 


async def delete_todo(todo: DeleteTodo, response: Response):
    try:
        await delete_todo_by_id(todo.id)
    except ValueError as e:
        (msg,) = e.args
        if msg == 'There is no Todo with the reference id.':
            response.status_code=status.HTTP_404_NOT_FOUND
            return Status(
                status = StatusEnum.FAIL,
                msg = msg
            )
        
        raise e

    return Status(
        status = StatusEnum.DELETED
    )
