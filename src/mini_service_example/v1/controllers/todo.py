from sqlalchemy import desc
from sqlalchemy.future import select
from sqlalchemy.orm import selectinload, joinedload
from ...database import get_async_scoped_session
from ..models.todo import (
    CreateTodo as CreateTodoAPI,
    Todo as TodoAPI,
    UpdateTodo as UpdateTodoAPI
)
from ...database.models.todo import Todo as TodoDB
from ...database.models.todo_history import TodoHistory as TodoHistoryDB


async def create_todo(todo: CreateTodoAPI):
    db_session = await get_async_scoped_session()

    todoDb = TodoDB()
    todoHistoryDb = TodoHistoryDB(
        title = todo.title,
        description = todo.description,
        todo = todoDb
    )

    db_session.add(todoDb)
    db_session.add(todoHistoryDb)
    await db_session.commit()
    await db_session.close()


async def get_todo_by_id(todo_id: int):
    db_session = await get_async_scoped_session()

    stmt = select(TodoDB, TodoHistoryDB).filter(TodoDB.id == todo_id).options(joinedload(TodoDB.todo_history)).order_by(desc(TodoHistoryDB.timestamp))
    query_result = await db_session.execute(stmt)
    todoDb = query_result.scalars().unique().one_or_none()

    response_todo = None

    if todoDb is not None:
        response_todo_first_entry = todoDb.todo_history[0]
        response_todo_last_entry = todoDb.todo_history[-1]

        response_todo = TodoAPI(
            created_at=response_todo_first_entry.timestamp,
            updated_at=response_todo_last_entry.timestamp,
            id = todoDb.id,
            title = response_todo_last_entry.title,
            description = response_todo_last_entry.description
        )

    await db_session.close()

    return response_todo


async def get_todos():
    db_session = await get_async_scoped_session()

    # query_result = await db_session.execute(select(TodoDB))
    # todosDb = query_result.scalars().all()

    stmt = select(TodoDB, TodoHistoryDB).options(joinedload(TodoDB.todo_history)).order_by(desc(TodoHistoryDB.timestamp))
    query_result = await db_session.execute(stmt)
    todosDb = query_result.scalars().unique().all()

    response = []

    for t in todosDb:
        response_todo_first_entry = t.todo_history[0]
        response_todo_last_entry = t.todo_history[-1]

        response.append(
            TodoAPI(
                created_at=response_todo_first_entry.timestamp,
                updated_at=response_todo_last_entry.timestamp,
                id=response_todo_last_entry.todo_id,
                title=response_todo_last_entry.title,
                description=response_todo_last_entry.description
            )
        )

    await db_session.close()

    return response


async def update_todo_by_id(todo: UpdateTodoAPI):
    db_session = await get_async_scoped_session()

    query_result = await db_session.execute(select(TodoDB)
        .filter(TodoDB.id == todo.id))
    todoDb = query_result.scalars().one_or_none()

    if todo.title is not None:
        todoDb.title = todo.title
    if todo.description is not None:
        todoDb.description = todo.description

    # todoHistoryDb = TodoHistoryDB(
    #     title = todo.title,
    #     description = todo.description,
    #     todo = todoDb
    # )

    db_session.add(todoDb)
    await db_session.commit()
    await db_session.close()


async def delete_todo_by_id(todo_id: int):
    db_session = await get_async_scoped_session()

    stmt = select(TodoDB, TodoHistoryDB).filter(TodoDB.id == todo_id).options(joinedload(TodoDB.todo_history)).order_by(desc(TodoHistoryDB.timestamp))
    query_result = await db_session.execute(stmt)
    todoDb = query_result.scalars().unique().one_or_none()

    if todoDb is None:
        await db_session.close()
        raise ValueError("There is no Todo with the reference id.")

    for history_item in todoDb.todo_history:
        await db_session.delete(history_item)

    await db_session.delete(todoDb)

    await db_session.commit()
    await db_session.close()
