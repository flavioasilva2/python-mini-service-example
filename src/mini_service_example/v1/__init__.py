from typing import List
from fastapi import APIRouter
from .handlers.todo import (
    get_todo,
    post_todo,
    get_all_todos,
    update_todo,
    delete_todo
)
from .models.todo import (
    Status,
    Todo
)


router = APIRouter(
    prefix='/v1'
)


router.add_api_route(
    '/todo',
    methods=['GET'],
    endpoint=get_todo,
    response_model=Status
)


router.add_api_route(
    '/todos',
    methods=['GET'],
    endpoint=get_all_todos,
    response_model=Status
)


router.add_api_route(
    '/todo',
    methods=['POST'],
    endpoint=post_todo,
    response_model=Status
)


router.add_api_route(
    '/todo',
    methods=['PUT'],
    endpoint=update_todo,
    response_model=Status
)


router.add_api_route(
    '/todo',
    methods=['DELETE'],
    endpoint=delete_todo,
    response_model=Status
)
