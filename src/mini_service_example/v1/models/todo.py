from pydantic import BaseModel
from datetime import datetime
from enum import Enum
from typing import List, Optional

class Todo(BaseModel):
    created_at: datetime
    updated_at: datetime
    id: int
    title: str
    description: str


class CreateTodo(BaseModel):
    title: str
    description: str


class UpdateTodo(BaseModel):
    id: int
    title: Optional[str]
    description: Optional[str]


class DeleteTodo(BaseModel):
    id: int


class StatusEnum(str, Enum):
    CREATED = "CREATED"
    UPDATED = "UPDATED"
    DELETED = "DELETED"
    READED  = "READED"
    FAIL    = "FAIL"


class Status(BaseModel):
    status: StatusEnum
    msg: Optional[str]
    todos: Optional[List[Todo]]
