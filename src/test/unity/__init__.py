from unittest import (
    TestSuite,
    TextTestRunner
)
from .test_sanity import TestSettings


def suite():
    suite = TestSuite()
    suite.addTest(TestSettings())
    return suite


if __name__ == '__main__':
    runner = TextTestRunner()
    runner.run(suite())
