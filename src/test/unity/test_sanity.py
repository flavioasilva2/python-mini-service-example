from unittest import TestCase
from pydantic import BaseSettings
from mini_service_example.config import get_settings


class TestSettings(TestCase):
    def test_if_setting_is_instance_of_BaseSettings(self):
        self.assertIsInstance(get_settings(), BaseSettings)
