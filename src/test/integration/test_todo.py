from unittest import TestCase
from fastapi import FastAPI
from fastapi.testclient import TestClient
from mini_service_example import app


class TestTodo(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_inexisting_todo(self):
        with TestClient(app) as client:
            id_to_try = 1

            response = client.get(
                '/v1/todo',
                params={
                    'todo_id': id_to_try
                }
            )

            self.assertEqual(response.status_code, 400)
            self.assertEqual(
                response.json(),
                {
                    'status': 'FAIL',
                    'msg': f'Fail to recovery todo with id {id_to_try}',
                    'todos': None
                }
            )

    def test_get_existing_todo(self):
        with TestClient(app) as client:
            id_to_try = 1
            test_todo = {
                'id': id_to_try,
                'title': 'test todo',
                'description': 'this is a test todo'
            }

            response = client.get(
                '/v1/todo',
                params={
                    'todo_id': id_to_try
                }
            )

            resonse_json = response.json()

            self.assertEqual(resonse_json['status'], 'READED')
            self.assertEqual(resonse_json['todos'][0]['id'], test_todo['id'])
            self.assertEqual(resonse_json['todos'][0]['title'], test_todo['title'])
            self.assertEqual(resonse_json['todos'][0]['description'], test_todo['description'])
            self.assertEqual(response.status_code, 200)
