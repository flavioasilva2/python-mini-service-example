from unittest import (
    TestSuite,
    TextTestRunner
)


def suite():
    suite = TestSuite()
    return suite


if __name__ == '__main__':
    runner = TextTestRunner()
    runner.run(suite())
