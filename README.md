# Python mini service example with fastapi and sqlalchemy

Esse pacote é um exemplo de mini serviço REST escrito em python utilizando os seguintes frameworks:

* [FastAPI](https://fastapi.tiangolo.com/)

* [Sqlalchemy](https://www.sqlalchemy.org/)

* [Alembic](https://alembic.sqlalchemy.org/en/latest/)

* [Pytest](https://docs.pytest.org/en/7.1.x/)

* [Unittest](https://docs.python.org/3/library/unittest.html)

Foram implementados:

* Migrations

* Classes de ORM

* TDD

* BDD

## Configurando ambiente de desenvolvimento

Crie o ambiente virtual (venv)

```
python3 -m venv .venv
```

Ative o venv:

```
. ./.venv/bin/activate
```

Atualize o pip do venv

```
pip install -U pip
```

Instale o pacote em modo editável:

```
pip install -e '.[dev,test]'
```

Pronto o seu ambiente de desenvolvimento está configurado. Para executar utilize o seguinte comando:

```
FAST_API_ENV='devlopment' uvicorn mini_service_example:app --reload
```

Você pode acessar o swagger em http://localhost:8000/docs

Você pode editar os arquivos e o uvicorn automaticamente irá reiniciar a aplicação para incluir as novas alterações.

## Executando migrations

Para executar as migrations, certifique-se primeiramente que a string de
conexão com a base no arquivo alembic.ini está correta, e que o venv está
ativo. Por exemplo:

```
sqlalchemy.url = sqlite+aiosqlite:///mini_service_example.db
```

Se quiser visualizar o histórico de migrações use:
```
alembic history
```

Em seguida, para executar as migrations execute:
```
alembic upgrade head
```

Para adicionar uma nova migração, primeiro crie as classes correspondetes no sqlalchemy,
em seguida, certifique-se que o seu banco de de dados está atualizado com a migration _head_,
agora importe a classe modelo no arquivo alembic/env.py, crie a nova migration com o comando:

```
alembic revision --autogenerate -m "Comentario sobre a nova migration"
```

Agora revise o arquivo de migration que foi criado em alembic/versions, estando tudo certo aplique
a migration _head_.

## Preparando uma base de dados Postgres

Para configurar uma base de dados postgres para ser usada com esse serviço, basta utilizar os seguintes comandos no console postgres.

```
create database mini_service;
```

```
create user test with encrypted password 'test123';
```

```
grant all privileges on database mini_service to test;
```

Se a versão do postgres usada for >= 15 então execute o seguinte comando também:

```
alter database mini_service owner to test;
```

O código se encarregará de criar as tabelas e dar a carga mínima necessária ao funcionamento da aplicação.

## Configurnado acesso à banco de dados

Ao iniciar o serviço, automaticamente é criada uma conexão sqlite, no arquivo "mini_service_example.db". Para mudar o apontamento de banco de dados, basta redifinir a variável de ambiente *MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING*, por exempo:

Para sqlite:

```
MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING='sqlite+aiosqlite:///outroarquivo.db' FAST_API_ENV='devlopment' uvicorn mini_service_example:app --reload
```

Para Postgres:

```
MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING='postgresql+asyncpg://test:test123@localhost:5432/mini_service' FAST_API_ENV='devlopment' uvicorn mini_service_example:app --reload
```

## Gerando imagem docker

Para gerar uma imagem docker utilize o seguinte comando:

```
sudo docker build -t mini_service_example:latest .
```

## Executando a imagem docker

Modo interativo:
```
sudo docker run --rm -it -p 8000:8000 mini_service_example:latest
```

Em segundo plano:
```
sudo docker run -d -p 8000:8000 --name mini_service mini_service_example:latest
```

Para utilizar uma base de dados Postgres ao invéz da base de dados padrão sqlite, adicione a seguinte variável de ambiente com a string de conexão:

```
MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING='postgresql+asyncpg://test:test123@localhost:5432/mini_service'
```

Por exemplo:
```
sudo docker run -e MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING='postgresql+asyncpg://test:test123@localhost:5432/mini_service' --rm -it -p 8000:8000 mini_service_example:latest
```

ou

```
sudo docker run -e MINI_SERVICE_EXAMPLE_DB_CONNECTION_STRING='postgresql+asyncpg://test:test123@localhost:5432/mini_service' -d -p 8000:8000 --name mini_service mini_service_example:latest
```
