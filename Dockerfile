FROM python:3.8.15-alpine3.16 as builder

ENV PATH="/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/.local/bin"

RUN mkdir -pv /build/ /.local /.cache

COPY . /build/

RUN chown nobody:nogroup -R /build /.local /.cache

USER nobody

RUN python3 -m pip install --upgrade build pip \
 && cd /build                                  \
 && python3 -m build

FROM python:3.8.15-alpine3.16

ENV PATH="/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/srv/app/.local/bin"

COPY --from=builder /build/dist/mini_service_example-0.1.0-py3-none-any.whl /mini_service_example-0.1.0-py3-none-any.whl

RUN adduser -h /srv/app -s /bin/nologin -D app \
 && apk --no-cache add                          \
      gcc=11.2.1_git20220219-r2                 \
      musl-dev=1.2.3-r3                         \
      libffi-dev=3.4.2-r1

USER app

WORKDIR /srv/app

COPY alembic.ini /srv/app
COPY src/mini_service_example/database/migrations /srv/app/src/mini_service_example/database/migrations

RUN pip install -U pip \
 && pip install /mini_service_example-0.1.0-py3-none-any.whl

ENV FAST_API_ENV="production"

EXPOSE 8000

CMD ["uvicorn", "mini_service_example:app", "--host", "0.0.0.0", "--port", "8000"]
