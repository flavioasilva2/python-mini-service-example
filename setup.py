import setuptools

testing_packages = [
    'pytest>=7.1.3'
]

dev_packages = [
    'fastapi[all]==0.85.1'
]

prod_packages = [
    'fastapi==0.85.1',
    'uvicorn[standard]==0.18.3',
    'SQLAlchemy==1.4.42',
    'alembic==1.8.1',
    'aiosqlite==0.17.0',
    'asyncpg==0.26.0',
    'passlib==1.7.4',
    'jwcrypto==1.4.2',
    'pydantic[email]==1.10.2'
]


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

with open("LICENSE", "r", encoding="utf-8") as lic:
    project_license = lic.read()

setuptools.setup(
    name="mini_service_example",
    version="0.1.0",
    author="Flávio Amaral e Silva",
    author_email="flavio.asilva@gmail.com",
    description="Exemplo de mini serviço em python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license=project_license,
    url="https://gitlab.com/flavioasilva2/python-mini-service-example",
    project_urls={
        "Bug Tracker": "https://gitlab.com/flavioasilva2/python-mini-service-example/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    install_requires=prod_packages,
    extras_require={
        'dev': dev_packages,
        'test': testing_packages
    },
    python_requires=">=3.8"
)